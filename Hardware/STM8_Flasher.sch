EESchema Schematic File Version 4
LIBS:STM8_Flasher-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR01
U 1 1 59C0F95C
P 5650 4650
F 0 "#PWR01" H 5650 4400 50  0001 C CNN
F 1 "GND" H 5655 4477 50  0000 C CNN
F 2 "" H 5650 4650 50  0001 C CNN
F 3 "" H 5650 4650 50  0001 C CNN
	1    5650 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 4650 5650 4500
Text GLabel 8900 5800 0    60   Input ~ 6
SWIM_NRST
Text GLabel 8850 5700 0    60   Input ~ 6
SWIM_OUT
Wire Wire Line
	7150 3900 6950 3900
Wire Wire Line
	6950 4000 7150 4000
Text GLabel 7150 3900 2    60   Input ~ 6
SWIM_NRST
$Comp
L power:GND #PWR02
U 1 1 59C10296
P 9600 6000
F 0 "#PWR02" H 9600 5750 50  0001 C CNN
F 1 "GND" H 9605 5827 50  0000 C CNN
F 2 "" H 9600 6000 50  0001 C CNN
F 3 "" H 9600 6000 50  0001 C CNN
	1    9600 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	9600 6000 9600 5900
$Comp
L power:GND #PWR06
U 1 1 59C1043A
P 4000 4650
F 0 "#PWR06" H 4000 4400 50  0001 C CNN
F 1 "GND" H 4005 4477 50  0000 C CNN
F 2 "" H 4000 4650 50  0001 C CNN
F 3 "" H 4000 4650 50  0001 C CNN
	1    4000 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 4650 4000 4550
Wire Wire Line
	4350 4200 4000 4200
Wire Wire Line
	4000 4200 4000 4250
Wire Wire Line
	3650 4250 3650 3100
Wire Wire Line
	3650 3100 4200 3100
$Comp
L power:GND #PWR05
U 1 1 59C107C3
P 3650 4650
F 0 "#PWR05" H 3650 4400 50  0001 C CNN
F 1 "GND" H 3655 4477 50  0000 C CNN
F 2 "" H 3650 4650 50  0001 C CNN
F 3 "" H 3650 4650 50  0001 C CNN
	1    3650 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 4650 3650 4550
Wire Wire Line
	6650 1750 6650 1900
$Comp
L power:+3.3V #PWR03
U 1 1 59C10A13
P 6650 1750
F 0 "#PWR03" H 6650 1600 50  0001 C CNN
F 1 "+3.3V" H 6665 1923 50  0000 C CNN
F 2 "" H 6650 1750 50  0001 C CNN
F 3 "" H 6650 1750 50  0001 C CNN
	1    6650 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 2350 6650 2200
$Comp
L power:+3.3V #PWR09
U 1 1 59C10D1C
P 5650 2700
F 0 "#PWR09" H 5650 2550 50  0001 C CNN
F 1 "+3.3V" H 5665 2873 50  0000 C CNN
F 2 "" H 5650 2700 50  0001 C CNN
F 3 "" H 5650 2700 50  0001 C CNN
	1    5650 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 2700 5650 2800
Text GLabel 2250 5200 2    60   Input ~ 6
UART_RX
Text GLabel 2250 5100 2    60   Input ~ 6
UART_TX
Wire Wire Line
	2050 5100 2250 5100
Wire Wire Line
	2050 5200 2250 5200
$Comp
L power:GND #PWR015
U 1 1 59C11386
P 2100 5350
F 0 "#PWR015" H 2100 5100 50  0001 C CNN
F 1 "GND" H 2105 5177 50  0000 C CNN
F 2 "" H 2100 5350 50  0001 C CNN
F 3 "" H 2100 5350 50  0001 C CNN
	1    2100 5350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 5350 2100 5000
Wire Wire Line
	2100 5000 2050 5000
Text GLabel 4250 3900 0    60   Input ~ 6
UART_RX
Wire Wire Line
	4250 3900 4350 3900
Text GLabel 4250 3800 0    60   Input ~ 6
UART_TX
Wire Wire Line
	4250 3800 4350 3800
Text GLabel 9400 2000 2    60   Input ~ 6
I2C_SDA
$Comp
L power:+3.3V #PWR011
U 1 1 59C120BF
P 8650 1700
F 0 "#PWR011" H 8650 1550 50  0001 C CNN
F 1 "+3.3V" H 8665 1873 50  0000 C CNN
F 2 "" H 8650 1700 50  0001 C CNN
F 3 "" H 8650 1700 50  0001 C CNN
	1    8650 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	8650 1700 8650 1800
$Comp
L power:GND #PWR012
U 1 1 59C122DA
P 8650 2550
F 0 "#PWR012" H 8650 2300 50  0001 C CNN
F 1 "GND" H 8655 2377 50  0000 C CNN
F 2 "" H 8650 2550 50  0001 C CNN
F 3 "" H 8650 2550 50  0001 C CNN
	1    8650 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	9300 1350 9300 1400
Wire Wire Line
	9300 1400 9100 1400
$Comp
L power:+3.3V #PWR014
U 1 1 59C1251C
P 9300 1350
F 0 "#PWR014" H 9300 1200 50  0001 C CNN
F 1 "+3.3V" H 9315 1523 50  0000 C CNN
F 2 "" H 9300 1350 50  0001 C CNN
F 3 "" H 9300 1350 50  0001 C CNN
	1    9300 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	8150 2000 8150 2100
Wire Wire Line
	8250 2000 8150 2000
Wire Wire Line
	8250 2200 8150 2200
Wire Wire Line
	8250 2100 8150 2100
Connection ~ 8150 2100
Text GLabel 7150 3600 2    60   Input ~ 6
I2C_SDA
Wire Wire Line
	6950 3600 7150 3600
Wire Wire Line
	6950 3500 7150 3500
Wire Wire Line
	9250 4150 9250 4050
Wire Wire Line
	8850 4150 8850 4050
Wire Wire Line
	9250 3650 9250 3750
Wire Wire Line
	8850 3650 9050 3650
Wire Wire Line
	8850 3650 8850 3750
$Comp
L power:+3.3V #PWR013
U 1 1 59C13885
P 9050 3550
F 0 "#PWR013" H 9050 3400 50  0001 C CNN
F 1 "+3.3V" H 9065 3723 50  0000 C CNN
F 2 "" H 9050 3550 50  0001 C CNN
F 3 "" H 9050 3550 50  0001 C CNN
	1    9050 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	9050 3550 9050 3650
Connection ~ 9050 3650
Text GLabel 8750 4550 0    60   Input ~ 6
LED_RED
Text GLabel 7150 3500 2    60   Input ~ 6
I2C_SCL
Text GLabel 9400 2100 2    60   Input ~ 6
I2C_SCL
Wire Wire Line
	8750 4550 8850 4550
Wire Wire Line
	8850 4550 8850 4450
Text GLabel 9350 4550 2    60   Input ~ 6
LED_GREEN
Wire Wire Line
	9250 4450 9250 4550
Wire Wire Line
	9250 4550 9350 4550
Text GLabel 7150 3200 2    60   Input ~ 6
LED_RED
Text GLabel 7150 3300 2    60   Input ~ 6
LED_GREEN
Wire Wire Line
	6950 3200 7150 3200
$Comp
L power:GND #PWR010
U 1 1 59C14E89
P 3050 3850
F 0 "#PWR010" H 3050 3600 50  0001 C CNN
F 1 "GND" H 3055 3677 50  0000 C CNN
F 2 "" H 3050 3850 50  0001 C CNN
F 3 "" H 3050 3850 50  0001 C CNN
	1    3050 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 3850 3050 3450
Wire Wire Line
	3050 3450 2950 3450
Text GLabel 4350 3700 0    60   Input ~ 6
PROG_SWITCH
Wire Wire Line
	2400 3450 2550 3450
Wire Wire Line
	1650 3450 1900 3450
Wire Wire Line
	1900 3300 1900 3450
Connection ~ 1900 3450
$Comp
L power:GND #PWR08
U 1 1 59C1562F
P 1900 3900
F 0 "#PWR08" H 1900 3650 50  0001 C CNN
F 1 "GND" H 1905 3727 50  0000 C CNN
F 2 "" H 1900 3900 50  0001 C CNN
F 3 "" H 1900 3900 50  0001 C CNN
	1    1900 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	1900 3800 1900 3900
$Comp
L power:+3.3V #PWR07
U 1 1 59C159E6
P 1900 2950
F 0 "#PWR07" H 1900 2800 50  0001 C CNN
F 1 "+3.3V" H 1915 3123 50  0000 C CNN
F 2 "" H 1900 2950 50  0001 C CNN
F 3 "" H 1900 2950 50  0001 C CNN
	1    1900 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1900 2950 1900 3000
Wire Wire Line
	6950 3300 7150 3300
Text GLabel 1650 3450 0    60   Input ~ 6
PROG_SWITCH
$Comp
L power:VCC #PWR021
U 1 1 59C5404D
P 1650 1200
F 0 "#PWR021" H 1650 1050 50  0001 C CNN
F 1 "VCC" H 1667 1373 50  0000 C CNN
F 2 "" H 1650 1200 50  0001 C CNN
F 3 "" H 1650 1200 50  0001 C CNN
	1    1650 1200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR022
U 1 1 59C540F9
P 1650 1650
F 0 "#PWR022" H 1650 1400 50  0001 C CNN
F 1 "GND" H 1655 1477 50  0000 C CNN
F 2 "" H 1650 1650 50  0001 C CNN
F 3 "" H 1650 1650 50  0001 C CNN
	1    1650 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 1500 1650 1500
Wire Wire Line
	1650 1500 1650 1650
Wire Wire Line
	1450 1400 1650 1400
Wire Wire Line
	1650 1400 1650 1200
Wire Wire Line
	9600 5900 9750 5900
Wire Wire Line
	4050 2850 4100 2850
Wire Wire Line
	4100 3400 4350 3400
Wire Wire Line
	4200 2750 4050 2750
$Comp
L power:GND #PWR023
U 1 1 59CB84AF
P 4400 2700
F 0 "#PWR023" H 4400 2450 50  0001 C CNN
F 1 "GND" H 4405 2527 50  0000 C CNN
F 2 "" H 4400 2700 50  0001 C CNN
F 3 "" H 4400 2700 50  0001 C CNN
	1    4400 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 2700 4400 2650
Wire Wire Line
	4400 2650 4050 2650
Wire Wire Line
	4200 2750 4200 3100
Connection ~ 4200 3100
Wire Wire Line
	4100 2850 4100 3400
Wire Wire Line
	8150 2100 8150 2200
Wire Wire Line
	9050 3650 9250 3650
Wire Wire Line
	1900 3450 2100 3450
Wire Wire Line
	1900 3450 1900 3500
Wire Wire Line
	4200 3100 4350 3100
$Comp
L Device:C C3
U 1 1 5C744B8E
P 6650 2050
F 0 "C3" H 6765 2096 50  0000 L CNN
F 1 "1u" H 6765 2005 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6688 1900 50  0001 C CNN
F 3 "~" H 6650 2050 50  0001 C CNN
	1    6650 2050
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5C7454F2
P 1900 3150
F 0 "R1" H 1970 3196 50  0000 L CNN
F 1 "10k" H 1970 3105 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1830 3150 50  0001 C CNN
F 3 "~" H 1900 3150 50  0001 C CNN
	1    1900 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C6
U 1 1 5C745B68
P 1900 3650
F 0 "C6" H 2015 3696 50  0000 L CNN
F 1 "0.1u" H 2015 3605 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1938 3500 50  0001 C CNN
F 3 "~" H 1900 3650 50  0001 C CNN
	1    1900 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5C746498
P 2250 3450
F 0 "R2" V 2043 3450 50  0000 C CNN
F 1 "1k" V 2134 3450 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2180 3450 50  0001 C CNN
F 3 "~" H 2250 3450 50  0001 C CNN
	1    2250 3450
	0    1    1    0   
$EndComp
$Comp
L Switch:SW_Push SW1
U 1 1 5C746D13
P 2750 3450
F 0 "SW1" H 2750 3735 50  0000 C CNN
F 1 "SW_Push" H 2750 3644 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 2750 3650 50  0001 C CNN
F 3 "~" H 2750 3650 50  0001 C CNN
	1    2750 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	9400 2000 9100 2000
Wire Wire Line
	9050 2100 9300 2100
Wire Wire Line
	8150 2200 8150 2450
Connection ~ 8150 2200
Wire Wire Line
	8650 2450 8650 2550
Wire Wire Line
	9100 2200 9100 2450
Wire Wire Line
	9100 2450 8650 2450
Wire Wire Line
	9050 2200 9100 2200
Wire Wire Line
	8150 2450 8650 2450
Connection ~ 8650 2450
Wire Wire Line
	8650 2400 8650 2450
$Comp
L Memory_EEPROM:24LC256 U2
U 1 1 5C74AD66
P 8650 2100
F 0 "U2" H 8650 2581 50  0000 C CNN
F 1 "24LC256" H 8350 2400 50  0000 C CNN
F 2 "Package_SO:TSSOP-8_4.4x3mm_P0.65mm" H 8650 2100 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/devicedoc/21203m.pdf" H 8650 2100 50  0001 C CNN
	1    8650 2100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R7
U 1 1 5C76BF7E
P 9100 1700
F 0 "R7" H 8950 1750 50  0000 L CNN
F 1 "4.7k" H 8850 1650 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 9030 1700 50  0001 C CNN
F 3 "~" H 9100 1700 50  0001 C CNN
	1    9100 1700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R8
U 1 1 5C76D110
P 9300 1700
F 0 "R8" H 9370 1746 50  0000 L CNN
F 1 "4.7k" H 9370 1655 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 9230 1700 50  0001 C CNN
F 3 "~" H 9300 1700 50  0001 C CNN
	1    9300 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	9100 1400 9100 1550
Wire Wire Line
	9100 1850 9100 2000
Connection ~ 9100 2000
Wire Wire Line
	9100 2000 9050 2000
Wire Wire Line
	9300 1850 9300 2100
Connection ~ 9300 2100
Wire Wire Line
	9300 2100 9400 2100
Wire Wire Line
	9300 1550 9300 1400
Connection ~ 9300 1400
$Comp
L Connector_Generic:Conn_01x03 J4
U 1 1 5C747F45
P 1850 5100
F 0 "J4" H 1768 4775 50  0000 C CNN
F 1 "UART_Port" H 1768 4866 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 1850 5100 50  0001 C CNN
F 3 "~" H 1850 5100 50  0001 C CNN
	1    1850 5100
	-1   0    0    1   
$EndComp
$Comp
L Device:R R5
U 1 1 5C74CB3B
P 9450 5700
F 0 "R5" H 9300 5750 50  0000 L CNN
F 1 "100" H 9200 5650 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 9380 5700 50  0001 C CNN
F 3 "~" H 9450 5700 50  0001 C CNN
	1    9450 5700
	0    1    1    0   
$EndComp
$Comp
L Device:R R6
U 1 1 5C74D932
P 9450 5800
F 0 "R6" H 9300 5850 50  0000 L CNN
F 1 "100" H 9200 5750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 9380 5800 50  0001 C CNN
F 3 "~" H 9450 5800 50  0001 C CNN
	1    9450 5800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9600 5800 9750 5800
Wire Wire Line
	9300 5800 8900 5800
Wire Wire Line
	8850 5700 9050 5700
Wire Wire Line
	9600 5700 9750 5700
$Comp
L Device:R R3
U 1 1 5C75DFBC
P 9050 5450
F 0 "R3" H 8900 5500 50  0000 L CNN
F 1 "3.3k" H 8800 5400 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8980 5450 50  0001 C CNN
F 3 "~" H 9050 5450 50  0001 C CNN
	1    9050 5450
	-1   0    0    1   
$EndComp
Wire Wire Line
	9050 5600 9050 5700
Connection ~ 9050 5700
Wire Wire Line
	9050 5700 9300 5700
$Comp
L power:+3.3V #PWR0101
U 1 1 5C761671
P 9050 5200
F 0 "#PWR0101" H 9050 5050 50  0001 C CNN
F 1 "+3.3V" H 9065 5373 50  0000 C CNN
F 2 "" H 9050 5200 50  0001 C CNN
F 3 "" H 9050 5200 50  0001 C CNN
	1    9050 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	9050 5200 9050 5300
$Comp
L Device:LED D1
U 1 1 5C7693FA
P 8850 3900
F 0 "D1" V 8889 3783 50  0000 R CNN
F 1 "LED" V 8798 3783 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 8850 3900 50  0001 C CNN
F 3 "~" H 8850 3900 50  0001 C CNN
	1    8850 3900
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D2
U 1 1 5C76A74A
P 9250 3900
F 0 "D2" V 9289 3783 50  0000 R CNN
F 1 "LED" V 9198 3783 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 9250 3900 50  0001 C CNN
F 3 "~" H 9250 3900 50  0001 C CNN
	1    9250 3900
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R9
U 1 1 5C770BA5
P 9250 4300
F 0 "R9" H 9100 4350 50  0000 L CNN
F 1 "1k" H 9000 4250 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 9180 4300 50  0001 C CNN
F 3 "~" H 9250 4300 50  0001 C CNN
	1    9250 4300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 5C7711C1
P 8850 4300
F 0 "R4" H 8700 4350 50  0000 L CNN
F 1 "1k" H 8600 4250 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8780 4300 50  0001 C CNN
F 3 "~" H 8850 4300 50  0001 C CNN
	1    8850 4300
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J2
U 1 1 5C77162E
P 3850 2750
F 0 "J2" H 3768 2425 50  0000 C CNN
F 1 "ISP_Port" H 3768 2516 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 3850 2750 50  0001 C CNN
F 3 "~" H 3850 2750 50  0001 C CNN
	1    3850 2750
	-1   0    0    1   
$EndComp
$Comp
L Device:C C5
U 1 1 5C7726B6
P 4000 4400
F 0 "C5" H 4115 4446 50  0000 L CNN
F 1 "1u" H 4115 4355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4038 4250 50  0001 C CNN
F 3 "~" H 4000 4400 50  0001 C CNN
	1    4000 4400
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 5C772D14
P 3650 4400
F 0 "C4" H 3765 4446 50  0000 L CNN
F 1 "10u" H 3765 4355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3688 4250 50  0001 C CNN
F 3 "~" H 3650 4400 50  0001 C CNN
	1    3650 4400
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J3
U 1 1 5C7736FF
P 1250 1500
F 0 "J3" H 1168 1175 50  0000 C CNN
F 1 "Power" H 1168 1266 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1250 1500 50  0001 C CNN
F 3 "~" H 1250 1500 50  0001 C CNN
	1    1250 1500
	-1   0    0    1   
$EndComp
$Comp
L stm8:STM8S003F3P IC1
U 1 1 5C774B28
P 5650 3600
F 0 "IC1" H 5800 4300 50  0000 C CNN
F 1 "STM8S003F3P" H 5250 4300 50  0000 C CNN
F 2 "Package_SO:TSSOP-20_4.4x6.5mm_P0.65mm" H 5650 2900 50  0000 C CIN
F 3 "" H 5600 3200 50  0000 C CNN
	1    5650 3600
	1    0    0    -1  
$EndComp
Text GLabel 7150 4000 2    60   Input ~ 6
SWIM_OUT
$Comp
L Connector_Generic:Conn_01x03 J1
U 1 1 5C756BF0
P 9950 5800
F 0 "J1" H 10200 5750 50  0000 C CNN
F 1 "SWIM_Port" H 10200 5850 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 9950 5800 50  0001 C CNN
F 3 "~" H 9950 5800 50  0001 C CNN
	1    9950 5800
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5C7446C2
P 2200 1500
F 0 "C1" H 2315 1546 50  0000 L CNN
F 1 "1u" H 2315 1455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 2238 1350 50  0001 C CNN
F 3 "~" H 2200 1500 50  0001 C CNN
	1    2200 1500
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5C7440F1
P 3350 1500
F 0 "C2" H 3465 1546 50  0000 L CNN
F 1 "1u" H 3465 1455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3388 1350 50  0001 C CNN
F 3 "~" H 3350 1500 50  0001 C CNN
	1    3350 1500
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:MIC5205-3.3YM5 U1
U 1 1 5C740BD2
P 2900 1350
F 0 "U1" H 2900 1692 50  0000 C CNN
F 1 "MIC5205-3.3YM5" H 2900 1601 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 2900 1675 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/20005785A.pdf" H 2900 1350 50  0001 C CNN
	1    2900 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 1250 2200 1350
Wire Wire Line
	3350 1250 3350 1350
Wire Wire Line
	2400 1250 2600 1250
Connection ~ 2200 1250
Wire Wire Line
	2200 1750 2200 1650
Connection ~ 3350 1250
Wire Wire Line
	3350 1750 3350 1650
$Comp
L power:GND #PWR020
U 1 1 59C536A8
P 3350 1750
F 0 "#PWR020" H 3350 1500 50  0001 C CNN
F 1 "GND" H 3355 1577 50  0000 C CNN
F 2 "" H 3350 1750 50  0001 C CNN
F 3 "" H 3350 1750 50  0001 C CNN
	1    3350 1750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR019
U 1 1 59C535D1
P 2200 1750
F 0 "#PWR019" H 2200 1500 50  0001 C CNN
F 1 "GND" H 2205 1577 50  0000 C CNN
F 2 "" H 2200 1750 50  0001 C CNN
F 3 "" H 2200 1750 50  0001 C CNN
	1    2200 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 1200 3350 1250
Wire Wire Line
	3200 1250 3350 1250
$Comp
L power:+3.3V #PWR018
U 1 1 59C53156
P 3350 1200
F 0 "#PWR018" H 3350 1050 50  0001 C CNN
F 1 "+3.3V" H 3365 1373 50  0000 C CNN
F 2 "" H 3350 1200 50  0001 C CNN
F 3 "" H 3350 1200 50  0001 C CNN
	1    3350 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 1750 2900 1650
$Comp
L power:GND #PWR017
U 1 1 59C52EC4
P 2900 1750
F 0 "#PWR017" H 2900 1500 50  0001 C CNN
F 1 "GND" H 2905 1577 50  0000 C CNN
F 2 "" H 2900 1750 50  0001 C CNN
F 3 "" H 2900 1750 50  0001 C CNN
	1    2900 1750
	1    0    0    -1  
$EndComp
Connection ~ 2400 1250
Wire Wire Line
	2200 1150 2200 1250
$Comp
L power:VCC #PWR016
U 1 1 59C52BA0
P 2200 1150
F 0 "#PWR016" H 2200 1000 50  0001 C CNN
F 1 "VCC" H 2217 1323 50  0000 C CNN
F 2 "" H 2200 1150 50  0001 C CNN
F 3 "" H 2200 1150 50  0001 C CNN
	1    2200 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 1350 2600 1350
Wire Wire Line
	2400 1250 2400 1350
Wire Wire Line
	2200 1250 2400 1250
$Comp
L power:GND #PWR04
U 1 1 59C108BF
P 6650 2350
F 0 "#PWR04" H 6650 2100 50  0001 C CNN
F 1 "GND" H 6655 2177 50  0000 C CNN
F 2 "" H 6650 2350 50  0001 C CNN
F 3 "" H 6650 2350 50  0001 C CNN
	1    6650 2350
	1    0    0    -1  
$EndComp
$EndSCHEMATC
